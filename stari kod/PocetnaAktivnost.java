package knez.assdroid;

import java.io.File;

import knez.assdroid.logika.FormatValidator;

import com.lamerman.FileDialog;
import com.lamerman.FileDialogOptions;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class PocetnaAktivnost extends Activity implements OnClickListener {

	private Button dugmeExit, dugmePodesavanja, dugmeCreate, dugmeLoad;
	private static final int RQ_CODE_FILE_DIALOG = 1;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// redirektuje poziv editoru ako je aktivnost pokrenuta otvaranjem ass fajla
		if(getIntent() != null && getIntent().getAction() != null && getIntent().getData() != null) {
			if(getIntent().getAction().equals(Intent.ACTION_VIEW)) {
				pokreniEditor(getIntent().getData());
			}
		}
		
		setContentView(R.layout.activity_pocetna_aktivnost);
		pokupiPoglede();
		dodajListenere();
	}

	private void pokupiPoglede() {
		dugmeExit = (Button) findViewById(R.id.pocetna_dugme_exit);
		dugmeCreate = (Button) findViewById(R.id.pocetna_dugme_kreiraj);
		dugmeLoad = (Button) findViewById(R.id.pocetna_dugme_ucitaj);
		dugmePodesavanja = (Button) findViewById(R.id.pocetna_dugme_podesavanja);
	}

	private void dodajListenere() {
		dugmeExit.setOnClickListener(this);
		dugmeCreate.setOnClickListener(this);
		dugmeLoad.setOnClickListener(this);
		dugmePodesavanja.setOnClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_pocetna_aktivnost, menu);
		return true;
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		switch (item.getItemId()) {
		case R.id.meni_standard_create:
			kreirajPrevod();
			break;
		case R.id.meni_standard_load:
			ucitajPrevod();
			break;
		case R.id.meni_standard_podesavanja:
			prikaziPodesavanja();
			break;
		default:
			return false;
		}
		return true;
	}

	@Override
	public void onClick(View arg0) {
		switch(arg0.getId()) {
		case R.id.pocetna_dugme_exit:
			finish();
			break;
		case R.id.pocetna_dugme_kreiraj:
			kreirajPrevod();
			break;
		case R.id.pocetna_dugme_podesavanja:
			prikaziPodesavanja();
			break;
		case R.id.pocetna_dugme_ucitaj:
			ucitajPrevod();
			break;
		}		
	}


	private void ucitajPrevod() {
		Intent namera = new Intent(this.getBaseContext(), FileDialog.class);
		//TODO 
		// proveri jel ima SD card
			// ako nema, prikazi poruku
		// proveri zadnji folder u kom je otvarano nesto, ako ga nema otvori mnt/sdcard
		startActivityForResult(namera, RQ_CODE_FILE_DIALOG);
	}
	
	private void onZatvorenFajlDijalog(int resultCode, Intent data) {
		if(resultCode == RESULT_OK) {
			if(!data.hasExtra(FileDialogOptions.RESULT_FILE))
				return;
			
			String putanja = data.getStringExtra(FileDialogOptions.RESULT_FILE);
			if(!FormatValidator.formatPrihvatljiv(putanja)) {
				return;
			}
			
			Uri uriPutanja = Uri.fromFile(new File(putanja));
			pokreniEditor(uriPutanja);
		}
	}

	private void kreirajPrevod() {
		//the fuck ces ovde majke ti?
	}


	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch(requestCode) {
		case RQ_CODE_FILE_DIALOG:
			onZatvorenFajlDijalog(resultCode, data);
		}
	}

	private void prikaziPodesavanja() {
		Intent namera = new Intent(this,Podesavanja.class);
		startActivity(namera);
	}
	
	private void pokreniEditor(Uri putanjaFajla) {
		Intent editor = new Intent(this.getBaseContext(), EditorAktivnost.class);
		editor.setData(putanjaFajla);
		startActivity(editor);
	}


}
