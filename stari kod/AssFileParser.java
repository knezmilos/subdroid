package knez.assdroid.logika;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import knez.assdroid.util.PrevodilackoFormatiranje;

import android.database.Cursor;
import android.net.Uri;

public class AssFileParser implements Parser {
	
	private ParserCallback kolbek;
	
	private static final String UTF_BOM = "\uFEFF";

	private static final String SEKCIJA_PREVOD = "[Events]";
	private static final String SEKCIJA_PREVOD_DEFAULT_FORMAT = 
			"Format: Layer, Start, End, Style, Name, MarginL, MarginR, MarginV, Effect, Text";
	private static final String SEKCIJA_PREVOD_RED_FORMAT = "Format:";
	private static final String SEKCIJA_PREVOD_RED_COMMENT = "Comment:";
	private static final String SEKCIJA_PREVOD_RED_DIALOGUE = "Dialogue:";
	
	private static final String SEKCIJA_PREVOD_EL_LAYER = "Layer";
	private static final String SEKCIJA_PREVOD_EL_START = "Start";
	private static final String SEKCIJA_PREVOD_EL_END = "End";
	private static final String SEKCIJA_PREVOD_EL_STYLE = "Style";
	private static final String SEKCIJA_PREVOD_EL_NAME = "Name";
	private static final String SEKCIJA_PREVOD_EL_MARGINL = "MarginL";
	private static final String SEKCIJA_PREVOD_EL_MARGINR = "MarginR";
	private static final String SEKCIJA_PREVOD_EL_MARGINV = "MarginV";
	private static final String SEKCIJA_PREVOD_EL_EFFECT = "Effect";
	private static final String SEKCIJA_PREVOD_EL_TEXT = "Text";
	
	private static final String SEKCIJA_STIL = "[V4+ Styles]";
	private static final String SEKCIJA_STIL_OLD = "[V4 Styles]";
	
	private static final String SEKCIJA_ZAGLAVLJE = "[Script Info]";

	public AssFileParser(ParserCallback kolbek) {
		this.kolbek = kolbek;
	}

	/** Ucitava prevod sa tekuce putanje o kojem ce se ovaj handler starati. 
	 * @throws ParsiranjeException Ako dodje do problema pri parsiranju, usled pogresnog formata prevoda. */
	public void zapocniParsiranje(Uri uri) throws IOException, ParsiranjeException {
		List<String> sveZivo = ucitajSveZivo(uri.getPath());
		List<List<String>> sekcije = dajSekcije(sveZivo);		
		ubaciUPrevod(sekcije);
	}

	/** Iz ASS fajla ucitava sve redove koji nisu prazni/komentar i trpa ih u jednu listu */
	private List<String> ucitajSveZivo(String putanja) throws IOException {
		List<String> sveZivo = new LinkedList<String>(); 

		BufferedReader citac = new BufferedReader(new InputStreamReader(new FileInputStream(putanja)));
		boolean prvaLinija = true;
		try {
			while (true) {
				String linija = citac.readLine();
				if (linija == null) break;
				if(prvaLinija) {
					prvaLinija = false;
					if(linija.startsWith(UTF_BOM))
						linija = linija.substring(1);	
				}
				if (linija.trim().equals("") || linija.startsWith(";")) continue;
				sveZivo.add(linija);
			}
			return sveZivo;
		} finally {
			citac.close();
		}
	}

	private List<List<String>> dajSekcije(List<String> sveZivo) {
		List<List<String>> sekcije = new LinkedList<List<String>>();
		int pocetak = -1;
		for(int i = 0; i <sveZivo.size(); i++) {
			if(!sveZivo.get(i).startsWith("[")) continue;
			else {
				if(pocetak == -1)
					pocetak = i;
				else {
					sekcije.add(new LinkedList<String>(sveZivo.subList(pocetak, i)));
					pocetak = i;
				}
			}
		}
		// ako je neka sekcija otpoceta na poslednjem elementu... pa u njoj nema nista
		// ali ako je otpoceta negde ranije a nije zavrsena (sto ce se desavati stalno), onda je sve to ta sekcija
		if(pocetak != sveZivo.size() -1) {
			sekcije.add(new LinkedList<String>(sveZivo.subList(pocetak, sveZivo.size())));
		}

		return sekcije;
	}

	private void ubaciUPrevod(List<List<String>> sekcije) throws ParsiranjeException {
		// sad idem kroz sekcije
		for(int i = 0; i <sekcije.size(); i++) {
			if(sekcije.get(i).get(0).toLowerCase().equals(SEKCIJA_PREVOD.toLowerCase())) 
				ugrabiPrevode(sekcije.get(i));
			else if(sekcije.get(i).get(0).toLowerCase().equals(SEKCIJA_STIL.toLowerCase())
					|| sekcije.get(i).get(0).toLowerCase().equals(SEKCIJA_STIL_OLD.toLowerCase())) 
				ugrabiStilove(sekcije.get(i));
			else if(sekcije.get(i).get(0).toLowerCase().equals(SEKCIJA_ZAGLAVLJE.toLowerCase())) 
				ugrabiZaglavlja(sekcije.get(i));
		}//TODO font
	}

	private void ugrabiPrevode(List<String> lista) throws ParsiranjeException {
		if(lista.size() <= 1) {
			//tj. ako ima samo [events] a nema Format: red
			throw new ParsiranjeException("Nema Format: red!"); //TODO tekst
		}
		lista.remove(0); // uklanjam [Events]
		String formatLinija = lista.remove(0);
		if(!formatLinija.startsWith(SEKCIJA_PREVOD_RED_FORMAT)) {
			throw new ParsiranjeException("Nema Format: red!");
		}
		
		formatLinija = formatLinija.substring(SEKCIJA_PREVOD_RED_FORMAT.length());
		String formatDelovi[] = formatLinija.split(",");
		
		Map<String, Integer> mapaDelova = new HashMap<String, Integer>();
		for(int i = 0; i < formatDelovi.length; i++) {
			mapaDelova.put(formatDelovi[i].trim(), i);
		}
		
		ArrayList<RedPrevoda> listaPrevoda = new ArrayList<RedPrevoda>();
		RedPrevoda redPrevoda;
		int brojReda = 1;
		for(String normalanRed : lista) {
			redPrevoda = new RedPrevoda();
			if(normalanRed.startsWith(SEKCIJA_PREVOD_RED_COMMENT)) {
				redPrevoda.komentar = true;
				normalanRed = normalanRed.substring(SEKCIJA_PREVOD_RED_COMMENT.length()).trim();
			} else if(normalanRed.startsWith(SEKCIJA_PREVOD_RED_DIALOGUE)) {
				redPrevoda.komentar = false;
				normalanRed = normalanRed.substring(SEKCIJA_PREVOD_RED_DIALOGUE.length()).trim();
			} else {
				continue;
				// TODO: skip linija, moze upozorenje na kraju?
			}
			
			int brojZareza = 0;
			int odeljaka = formatDelovi.length; //koliko odeljaka razdvojenih zarezima ima (dakle ima odeljaka-1 zareza)
			int zadnjiSplit = 0;
			List<String> parcad = new LinkedList<String>();
			for (int i = 0; i < normalanRed.length(); i++) {
				if (normalanRed.charAt(i) == ',') {
					parcad.add(normalanRed.substring(zadnjiSplit,i));
					zadnjiSplit = i+1;//plus 1 da bi preskocio i zarez koji sledi
					brojZareza++;
					if (brojZareza == odeljaka-1) {
						parcad.add(normalanRed.substring(i+1)); // takodje plus jedan da bi preskocio zarez
						break;
					}
				}
			}
			
			// jeste, zakomplikovao si. U mapi ti pise koje parce ide gde (posto bi mogli da budu izmesani)
			// jer se redosled definise u Format: redu
			// TODO vidi ako nema nesto od ovoga u Format: delu sta ce da bude
			
			try {
				redPrevoda.effect = parcad.get(mapaDelova.get(SEKCIJA_PREVOD_EL_EFFECT));
				redPrevoda.end = PrevodilackoFormatiranje.parsirajVreme(parcad.get(mapaDelova.get(SEKCIJA_PREVOD_EL_END)));
				redPrevoda.layer = Integer.parseInt(parcad.get(mapaDelova.get(SEKCIJA_PREVOD_EL_LAYER)));
				redPrevoda.marginL = Integer.parseInt(parcad.get(mapaDelova.get(SEKCIJA_PREVOD_EL_MARGINL)));
				redPrevoda.marginR = Integer.parseInt(parcad.get(mapaDelova.get(SEKCIJA_PREVOD_EL_MARGINR)));
				redPrevoda.marginV = Integer.parseInt(parcad.get(mapaDelova.get(SEKCIJA_PREVOD_EL_MARGINV)));
				redPrevoda.actorName = parcad.get(mapaDelova.get(SEKCIJA_PREVOD_EL_NAME));
				redPrevoda.start = PrevodilackoFormatiranje.parsirajVreme(parcad.get(mapaDelova.get(SEKCIJA_PREVOD_EL_START)));
				redPrevoda.style = parcad.get(mapaDelova.get(SEKCIJA_PREVOD_EL_STYLE));
				redPrevoda.text = parcad.get(mapaDelova.get(SEKCIJA_PREVOD_EL_TEXT));
				
				redPrevoda.lineNumber = brojReda++;
				listaPrevoda.add(redPrevoda);
			} catch (NumberFormatException numex) {
				throw new ParsiranjeException("numberformat bla"); //TODO
			}
		}
		kolbek.ucitaniRedoviPrevoda(listaPrevoda);
	}

	private void ugrabiStilove(List<String> lista) {
		lista.remove(0); // uklanjam []
		ArrayList<RedStila> redoviStilova = new ArrayList<RedStila>();
		for(int i=0; i < lista.size(); i++)
			redoviStilova.add(new RedStila(lista.get(i)));
		
		kolbek.ucitaniRedoviStila(redoviStilova);
	}

	private void ugrabiZaglavlja(List<String> lista) {
		lista.remove(0); // uklanjam [Script Info]
		ArrayList<RedZaglavlja> redoviZaglavlja = new ArrayList<RedZaglavlja>();
		for(int i=0; i < lista.size(); i++)
			redoviZaglavlja.add(new RedZaglavlja(lista.get(i)));
		
		kolbek.ucitaniRedoviZaglavlja(redoviZaglavlja);
	}
	
	@SuppressWarnings("unused")
	@Deprecated
	private void ocistiRazmakeElementima(String[] elementi) {
		for(int i = 0; i < elementi.length; i++) {
			elementi[i] = elementi[i].trim();
		}
	}
	
	
	public void snimiPrevod(String putanjaPrevoda, Cursor redoviZaglavlja, Cursor redoviStila, Cursor redoviPrevoda)
			throws FileNotFoundException {
		File fajl = new File(putanjaPrevoda);
		PrintWriter p = new PrintWriter(fajl);
		
		p.print(UTF_BOM);
		
		p.println(SEKCIJA_ZAGLAVLJE);
		while(redoviZaglavlja.moveToNext()) {
			p.println(RedZaglavlja.kreirajStringIzKursora(redoviZaglavlja));
		}
		p.println();
		
		p.println(SEKCIJA_STIL);
		while(redoviStila.moveToNext()) {
			p.println(RedStila.kreirajStringIzKursora(redoviStila)); //TODO: nece da moze. ili oce ako je AssRedStila
		}
		p.println();
		
		p.println(SEKCIJA_PREVOD);
		p.println(SEKCIJA_PREVOD_DEFAULT_FORMAT);
		while(redoviPrevoda.moveToNext()) {
			RedPrevoda red = RedPrevoda.kreirajIzKursora(redoviPrevoda);
			p.printf("%s%d,%s,%s,%s,%s,%04d,%04d,%04d,%s,%s\n", 
					(red.komentar? SEKCIJA_PREVOD_RED_COMMENT : SEKCIJA_PREVOD_RED_DIALOGUE) + " ",
					red.layer, 
					PrevodilackoFormatiranje.formatirajVreme(red.start), 
					PrevodilackoFormatiranje.formatirajVreme(red.end), 
					red.style, 
					red.actorName,
					red.marginL, 
					red.marginR, 
					red.marginV, 
					red.effect, 
					red.text);
		}
		p.close();
	}
}
