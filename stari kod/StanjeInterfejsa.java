package knez.assdroid;

import knez.assdroid.util.Aplikacija;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;

public class StanjeInterfejsa implements PaketPodesavanja {
	
	private static final String PREFERENCE_IME = "knez.assdroid.stanje_interfejsa";

	private static final String KLJUC_AKTIVAN_JEZICAK = "Crtanje.interfejs_aktivan_jezicak";
	private static final String KLJUC_INTERFEJS_MINIMIZIRAN = "default_stanje_kontrole_minimizirane";
	
	private static final int DEFAULT_NEAKTIVAN_JEZICAK = -1;
	
	protected Integer aktivanJezicak = null;
	protected Boolean interfejsMinimiziran = null;
	
	/** Ucitava podesavanja iz preferenci. Za nepostojeca podesavanja koristi default vrednosti definisane
	 *  u resursima aplikacije */
	@Override
	public void ucitajPodesavanja() {
		Resources resursi = Aplikacija.dajKontekst().getResources();
		SharedPreferences preference = Aplikacija.dajKontekst()
				.getSharedPreferences(PREFERENCE_IME, Context.MODE_PRIVATE);
		
		aktivanJezicak = preference.getInt(KLJUC_AKTIVAN_JEZICAK, DEFAULT_NEAKTIVAN_JEZICAK);
		interfejsMinimiziran = preference.getBoolean(KLJUC_INTERFEJS_MINIMIZIRAN, 
				resursi.getBoolean(R.bool.default_stanje_kontrole_minimizirane));
	}

	@Override
	public void ucitajDefaultVrednosti() {		
		aktivanJezicak = Integer.valueOf(DEFAULT_NEAKTIVAN_JEZICAK);
		interfejsMinimiziran = false;
	}
	
	@Override
	public void perzistirajSe() {
		SharedPreferences.Editor editor = Aplikacija.dajKontekst()
				.getSharedPreferences(PREFERENCE_IME, Context.MODE_PRIVATE).edit();
		
		if(aktivanJezicak != null)
			editor.putInt(KLJUC_AKTIVAN_JEZICAK, aktivanJezicak);
		if(interfejsMinimiziran != null)
			editor.putBoolean(KLJUC_INTERFEJS_MINIMIZIRAN, interfejsMinimiziran);
		
		editor.commit();
	}

}
