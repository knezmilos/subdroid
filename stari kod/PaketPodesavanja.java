package knez.assdroid;

/** Interfejs koji treba da implementiraju klase koje u sebi sadrze nekakva podesavanja
 *  i koje znaju da ucitaju ta podesavanja iz nekog skladista i perzistiraju ih. */
public interface PaketPodesavanja {
	public void perzistirajSe();
	public void ucitajPodesavanja();
	public void ucitajDefaultVrednosti();
}
