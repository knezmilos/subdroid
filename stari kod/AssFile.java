package knez.assdroid.logika;

import java.util.LinkedList;
import java.util.List;

import android.net.Uri;

public class AssFile {
	
	public List<RedPrevoda> redoviPrevoda;
	public List<String> redoviStilovi;
	public List<String> redoviZaglavlje;
	
	public String putanjaPrevoda;
	public String imePrevoda;
	public boolean prevodMenjan;
	
	public boolean imaoBOM = false;
	
	public AssFile(Uri putanjaPrevoda) {
		redoviPrevoda = new LinkedList<RedPrevoda>();
		redoviStilovi = new LinkedList<String>();
		redoviZaglavlje = new LinkedList<String>();
		setPutanjaPrevoda(putanjaPrevoda);
		prevodMenjan = false;
	}
	
	public AssFile() {
		redoviPrevoda = new LinkedList<RedPrevoda>();
		redoviStilovi = new LinkedList<String>();
		redoviZaglavlje = new LinkedList<String>();
		putanjaPrevoda = "";
		imePrevoda = "";
		prevodMenjan = false;
	}
	
	/** Setuje putanju i ime prevoda iz dobijenog Uri-ja. */
	private void setPutanjaPrevoda(Uri uri) {
		putanjaPrevoda = uri.getPath();
		imePrevoda = uri.getPathSegments().get(uri.getPathSegments().size()-1);
	}

}
