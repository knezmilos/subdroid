package knez.assdroid.logika;

public class ParsiranjeException extends Exception {
	
	private static final long serialVersionUID = 1L;

	public ParsiranjeException(String poruka) {
		super(poruka);
	}

}
