package knez.assdroid.logika;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import knez.assdroid.util.FormatValidator;

import android.database.Cursor;
import android.net.Uri;

public interface Parser {
	
	public void zapocniParsiranje(Uri uri) throws IOException, ParsiranjeException;
	public void snimiPrevod(String putanja, Cursor redoviZaglavlja, Cursor redoviStila, Cursor redoviPrevoda)
			throws FileNotFoundException;
	
	public interface ParserCallback {
		public void ucitaniRedoviPrevoda(List<RedPrevoda> redovi);
		public void ucitaniRedoviStila(List<RedStila> redovi);
		public void ucitaniRedoviZaglavlja(List<RedZaglavlja> redovi);
		public void zavrsenoParsiranje(boolean problemi, String warnString);
	}
	
	public static class Fabrika {
		private Fabrika() {}
		public static Parser dajParserZaFajl(String fajl, ParserCallback kolbek) {
			if(fajl.toLowerCase().endsWith(FormatValidator.EKSTENZIJA_ASS)) {
				return new AssFileParser(kolbek);
			} //TODO SRT
			else throw new RuntimeException();
		}
	}

}
